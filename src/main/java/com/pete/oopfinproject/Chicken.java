/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pete.oopfinproject;

/**
 *
 * @author DELL
 */
public class Chicken {

    @Override
    public String toString() {
        return "Chicken{" + "Name=" + Name + ", Age=" + Age + ", Gender=" + Gender + ", Date=" + Date + ", Month=" + Month + ", Time=" + Time + ", Competition=" + Competition + '}';
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String Month) {
        this.Month = Month;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    public String getCompetition() {
        return Competition;
    }

    public void setCompetition(String Competition) {
        this.Competition = Competition;
    }
    private String Name;
    private int Age;
    private String Gender;
    private String Date;
    private String Month;
    private String Time;
    private String Competition;

    public Chicken(String Name, int Age, String Gender, String Date, String Month, String Time, String Competition) {
        this.Name = Name;
        this.Age = Age;
        this.Gender = Gender;
        this.Date = Date;
        this.Month = Month;
        this.Time = Time;
        this.Competition = Competition;
    }
}
